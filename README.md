# Evaluierung der Echtzeitfähigkeit eines Raspberry Pi anhand von Audioeffekten <br/>(Evaluating the real-time capability of a Raspberry Pi using audio effects)

### at the Bachelor's Programme [Elektronik und Computer Engineering](https://www.fh-joanneum.at/elektronik-und-computer-engineering/bachelor/) of [FH JOANNEUM](https://www.fh-joanneum.at/) – University of Applied Sciences, Austria

## Table of contents
* [bachelor thesis](doc/Bachelorarbeit_Steinwender.pdf)
* [source code](src/)
    * [effects](src/effects/)
    * [envelopes](src/envelopes/)
    * [filters](src/filters/)
    * [oscillators](src/oscillators/)
    * [plotHelper](src/plot/)
    * [processors](src/processors/)
    * [tools](src/tools/)

## Requirements
The project is based on python 3, please install the newest version (see [howTo](https://realpython.com/installing-python/)).

The following python packages need to be installed:
- [*sounddevice*](https://pypi.org/project/sounddevice/) (Audio I/O)
- [*numpy*](https://pypi.org/project/numpy/) (Computing, Array operations)
- [*scipy*](https://pypi.org/project/scipy/) (Math, Signals)
- [*pad4pi*](https://pypi.org/project/pad4pi/) (Keypad)
- [*matplotlib*](https://pypi.org/project/matplotlib/) (Graphs)

Installing package via pip:
```console
python -m pip install <packageName>
```

## How to run it
[See](src/)

## Useful links and resources
//todo
