from effects.effects import Effect
from oscillators.oscillators import *
from filters.filters import *
from filters.filterCoeffs import *
from tools.helper import bipolarModulation, norm

class PhaseShifterParameters:
    def __init__(self, oscFreq=0.2, oscDepth=50.0, oscIntensity=75, isQuadOsc=False):
        self.oscFreq = oscFreq;	            # phaser LFO rate in Hz (0.02-20)
        self.oscDepth = oscDepth;           # phaser LFO depth in % (0-100)
        self.oscIntensity = oscIntensity;   # phaser feedback in % (0-100)
        self.isQuadOsc = isQuadOsc;	        # quad phase LFO flag

class PhaseShifterFilterStages:
    PHASER_STAGE_COUNT = 6
    # min/max freq Values
    STAGE0_MIN_F = 16.0
    STAGE0_MAX_F = 1600.0
    STAGE1_MIN_F = 33.0
    STAGE1_MAX_F = 3300.0
    STAGE2_MIN_F = 48.0
    STAGE2_MAX_F = 4800.0
    STAGE3_MIN_F = 98.0
    STAGE3_MAX_F = 9800.0
    STAGE4_MIN_F = 160.0
    STAGE4_MAX_F = 16000.0
    STAGE5_MIN_F = 260.0
    STAGE5_MAX_F = 20480.0

class PhaseShifter(Effect):
    def __init__(self, sampleRate, oscFreq = 5): # todo check oscFreq value
        self.osc = LowFrequencyTriangleOscillator(sampleRate, oscFreq)
        filterParams = BiquadFilterCoeffCalculatorParams(sampleRate, 100.0, boostCutGain=0.0)
        self.phaseShiftParams = PhaseShifterParameters(oscFreq=oscFreq)
        self.filter = [DirectBiquadFilter]
        self.filter = self._initFilter(filterParams)
        self.cnt = 0

    def _initFilter(self, filterParams: BiquadFilterCoeffCalculatorParams):
        filter = []
        for i in range(PhaseShifterFilterStages.PHASER_STAGE_COUNT):
            filter.append(BiquadAPF2Filter(filterParams))

        return filter
    
    def applyEffect(self, x: float) -> float:
        oscData = self.osc.getOscillator()

        depth = self.phaseShiftParams.oscDepth / 100.0
        modValue = oscData.oscOutput * depth
        
        modFc = bipolarModulation(modValue, PhaseShifterFilterStages.STAGE0_MIN_F, PhaseShifterFilterStages.STAGE0_MAX_F)
        self.filter[0].changeFCorner(fCorner=modFc)

        modFc = bipolarModulation(modValue, PhaseShifterFilterStages.STAGE1_MIN_F, PhaseShifterFilterStages.STAGE1_MAX_F)
        self.filter[1].changeFCorner(fCorner=modFc)

        modFc = bipolarModulation(modValue, PhaseShifterFilterStages.STAGE2_MIN_F, PhaseShifterFilterStages.STAGE2_MAX_F)
        self.filter[2].changeFCorner(fCorner=modFc)

        modFc = bipolarModulation(modValue, PhaseShifterFilterStages.STAGE3_MIN_F, PhaseShifterFilterStages.STAGE3_MAX_F)
        self.filter[3].changeFCorner(fCorner=modFc)

        modFc = bipolarModulation(modValue, PhaseShifterFilterStages.STAGE4_MIN_F, PhaseShifterFilterStages.STAGE4_MAX_F)
        self.filter[4].changeFCorner(fCorner=modFc)
        
        modFc = bipolarModulation(modValue, PhaseShifterFilterStages.STAGE5_MIN_F, PhaseShifterFilterStages.STAGE5_MAX_F)
        self.filter[5].changeFCorner(fCorner=modFc)

        # calcualte the gamma values (see Pirkle S. 375 F. 13.4)
        gamma1 = self.filter[5].getA0Coefficient()
        gamma2 = self.filter[4].getA0Coefficient() * gamma1
        gamma3 = self.filter[3].getA0Coefficient() * gamma2
        gamma4 = self.filter[2].getA0Coefficient() * gamma3
        gamma5 = self.filter[1].getA0Coefficient() * gamma4
        gamma6 = self.filter[0].getA0Coefficient() * gamma5

        k = self.phaseShiftParams.oscIntensity / 100.0 # todo: check intensity value
        alpha0 = 1.0 / (1.0 + k * gamma6)

        # create feedback (c) Pirkle
        # todo: cant be calc with current biquad algorithm
        Sn = gamma5*self.filter[0].getDelayedSum() + gamma4*self.filter[1].getDelayedSum() + gamma3*self.filter[2].getDelayedSum() + \
                gamma2*self.filter[3].getDelayedSum() + gamma1*self.filter[4].getDelayedSum() + self.filter[5].getDelayedSum()
        
        u = alpha0*(x + k * Sn)
        
        filtered = x * alpha0
        filtered = self.filter[0].applyFilter(u)
        filtered = self.filter[1].applyFilter(filtered)
        filtered = self.filter[2].applyFilter(filtered)
        filtered = self.filter[3].applyFilter(filtered)
        filtered = self.filter[4].applyFilter(filtered)
        filtered = self.filter[5].applyFilter(filtered)

        return 0.4 * x + 0.15 * filtered # todo: test this
