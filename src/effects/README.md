## Effects
This folder contains implementations of various effects.
The file [effects.py](effects.py) includes the effect base-class *Effect*, implemented by all effects. 

### Delay Effects
The digital delay effect as well as the modulated delay effects are implemented in [effects.py](effects.py). Both effect types use separated parameter classes for parameterization. 

Three different modulated delay effects are implemented, Flanger, Chorus and Vibrato.

### Filter Effect
The [filterEffect](filterEffects.py) contains six different filter (LP2, HP2, BP2, BP20dB, Notch, AP2). The effect was implemented to verify the functionality of all filter. The filter are applied one after another for a specified time.

### Phase Shifter
The [phaseShifter](phaseShifter.py) implements the basic concept of a phase shifter, containing several allpass filter for different frequencies. This effect was not integrated into the thesis, because of calculation errors, therefore it's not working yet. 

### Vocoder
The file [vocoder.py](vocoder.py) implements the concept of a phase vocoder and effects based on the vocoder. 
