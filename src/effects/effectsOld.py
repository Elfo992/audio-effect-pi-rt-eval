import numpy as np

class EffectOld:
    def applyEffect(self, audioData):
        pass

    def _shapeCheck(self, y):
        if len(y.shape) < 2:
            y = np.expand_dims(y, axis=1)
        return y

class EchoEffect(EffectOld):
    def __init__(self, sampleRate, echoDelay = 0.5):
        self.echoDelay = echoDelay
        self.sampleRate = sampleRate # todo: maybe add to applyEffect?

    def applyEffect(self, audioData):
        delaySamples = int(self.echoDelay * self.sampleRate)
        echoData = np.zeros(len(audioData))

        for i, sample in enumerate(audioData):
            echoSample = audioData[i - delaySamples] if i - delaySamples >= 0 else 0
            echoData[i] = np.clip(sample + echoSample, -32768, 32767)
        return self._shapeCheck(echoData)