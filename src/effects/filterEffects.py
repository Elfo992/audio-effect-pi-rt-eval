from effects.effects import Effect
from filters.filters import *

# just to verify the filter
class FilterEffect(Effect):
    FILTER_CNT = 6      # number of filters
    CHANGE_CNT = 2**16  # number of samples before changing filter
    
    def __init__(self, sampleRate):
        self.sampleRate = sampleRate
        self.filters = []
        lpFilter = BiquadLP2Filter(BiquadFilterCoeffCalculatorParams(sampleRate, fc=500, wetToDryPercentage=90))
        hpFilter = BiquadHP2Filter(BiquadFilterCoeffCalculatorParams(sampleRate, fc=5000, wetToDryPercentage=90))
        bpFilter = BiquadBPF2Filter(BiquadFilterCoeffCalculatorParams(sampleRate, fc=5000, qFactor=5, wetToDryPercentage=90))
        bp0Filter = BiquadBPF20GFilter(BiquadFilterCoeffCalculatorParams(sampleRate, fc=5000, wetToDryPercentage=90))
        notchFilter = BiquadNotchFilter(BiquadFilterCoeffCalculatorParams(sampleRate, fc=1500, wetToDryPercentage=90))
        apFilter = BiquadAPF2Filter(BiquadFilterCoeffCalculatorParams(sampleRate, fc=1500, wetToDryPercentage=90))
        self.filters.append(lpFilter)
        self.filters.append(hpFilter)
        self.filters.append(bpFilter)
        self.filters.append(bp0Filter)
        self.filters.append(notchFilter)
        self.filters.append(apFilter)
        self.counter = 1
        self.filterIndex = 0

    def applyEffect(self, x: float) -> float:
        if self.counter % self.CHANGE_CNT == 0:
            self.filterIndex = (self.filterIndex + 1) % self.FILTER_CNT

        self.counter += 1
        return self.filters[self.filterIndex].applyFilter(x)