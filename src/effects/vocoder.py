import numpy as np

from effects.effects import Effect
from tools.buffer import CircularBuffer
from tools.fft import *
from tools.stftWindow import * 

class PhaseVocoder(Effect):
    def __init__(self, frameLength: float, overlapPerc: float):
        self.frameLength = frameLength
        self.overlapPerc = overlapPerc                              # percentage of overlap 0-99%
        self.hopSamples = int(frameLength * (1 - overlapPerc / 100))     # number of samples per hop
        self.inputBuffer = np.zeros(frameLength)
        self.outputBuffer = np.zeros(frameLength)
        self.bufferSize = frameLength * 1.0         # todo: how large has the buffer to be?
        self.bufferIndex = 0
        self.window = getHannWindow(frameLength)
        self.normWindowGain = self._getNormWindowGain(self.window, overlapPerc)
        self.circInBuffer = CircularBuffer(frameLength)
        self.circOutBuffer = CircularBuffer(frameLength)
        self.outBufferIndex = 0

    def _getNormWindowGain(self, window, overlapPerc) -> float:
        winSum = 0.0
        for w in window:
            winSum += float(w)
        return 1.0 / (winSum) # should be overlapPerc / (winSum * 100), but sound better that way

    def applyEffectSingle(self, x: float) -> float:
        # add sample to inputBuffer
        self.inputBuffer[self.bufferIndex] = x
        y = self.outputBuffer[self.bufferIndex]
        self.bufferIndex += 1

        # if buffer is full, apply fft, effect, ifft
        if self.bufferIndex >= self.frameLength:
            # window the signal an apply the fft
            window = self.window
            fftInput = window * self.inputBuffer
            fftOutput = applyFFT(fftInput, self.frameLength)

            # apply an effect on the fft data
            effected = self.applyEffectFft(fftOutput)

            # do the ifft and window again
            self.outputBuffer = applyIFFT(effected, self.frameLength)
            self.bufferIndex = 0

        return y
    
    def applyEffect(self, x: float) -> float:
        self.circInBuffer.append(x)

        if self.circInBuffer.isFull == False:
            return x # or 0.0?
        
        if self.circInBuffer.end % self.hopSamples == 0:
            # window the signal an apply the fft
            fftInput = self.window * self.circInBuffer.getAllRight()
            fftOutput = applyFFT(fftInput, self.frameLength)

            # apply an effect on the fft data
            effected = self.applyEffectFft(fftOutput)

            # do the ifft and window again
            ifftOutput = applyIFFT(effected, self.frameLength)

            # normalize it, because we have much more energy from the overlapping
            ifftOutput *= 0.8 # self.normWindowGain

            # add to output buffer by multiplying to already existing value
            self.circOutBuffer.appendMultipleByAdding(ifftOutput)
        
        y = self.circOutBuffer.pop()
        return y
    
    def applyEffectFft(self, fftValues):
        pass

# 512 length sounds good, the higher the overlap, the higher the pitch <60 for rt
class PhaseVocoderRobot(PhaseVocoder):
    # to get a robot like effect, set the phase of all values to zero
    def applyEffectFft(self, fftValues):
        output = fftValues
        phaseRobot = 0.0

        for n in range(len(fftValues)):
            complexValue = complex(fftValues[n])
            magnitude = np.abs(complexValue)

            # convert back to complex number
            output[n] = complex(magnitude * np.cos(phaseRobot), magnitude * np.sin(phaseRobot))

        return output

# Pirkle S. 584 F. 20.15
class PhaseVocoderNoiseReducer(PhaseVocoder):
    R_SCALE = 0.1

    # apply a noise reduction by magnitude scaling
    def applyEffectFft(self, fftValues):
        output = fftValues

        for n in range(len(fftValues)):
            complexValue = complex(fftValues[n])
            magnitude = np.abs(complexValue)
            phase = np.angle(complexValue)
            
            scaleFactor = magnitude / (magnitude + self.R_SCALE)
            scaledMagn = magnitude * scaleFactor

            # convert back to complex number
            output[n] = complex(scaledMagn * np.cos(phase), scaledMagn * np.sin(phase))

        return output


class PhaseVocoderWhisper(PhaseVocoder):
    LOW_PHASE = 0.0
    HIGH_PHASE = np.pi
    
    # to get a whisper like effect, set the phase to random value 0-2pi
    def applyEffectFft(self, fftValues):
        output = fftValues
        length = len(fftValues)
        lRange = int(length / 2)
        # only iterate over the first half, because we want a real signal and can fill the second half wit conjugated values
        for n in range(lRange):
            complexValue = complex(fftValues[n])
            magnitude = np.abs(complexValue)

            rndPhase = np.random.uniform(self.LOW_PHASE, self.HIGH_PHASE)
            
            # convert back to complex number
            output[n] = complex(magnitude * np.cos(rndPhase), magnitude * np.sin(rndPhase))
            
            # maintain symmetry to get real output
            if n > 0 and n < lRange:
                output[length - n] = complex(magnitude * np.cos(rndPhase), -magnitude * np.sin(rndPhase))

        return output
    
class FrequencyPeakInformation():
    def __init__(self) -> None:
        self.isPeak = False         # defines if this is a picked peak
        self.magnitude = 0.0        # magnitude of the peak
        self.phase = 0.0            # phase of the peak
        self.adjustedPhase = 0.0    # phase corrected value
        self.peakPosition = 0       # index of the peak
        self.prevPeakPosition = 0   # index of peak in last window
        self.phaseUpdateValue = 0.0 # phase update value

class PhaseVocoderPitchShiftParams():
    def __init__(self, semitonesShift=0.0, enablePeakPhaseLocking=False, enablePeakTracking=False, fftFrameLength=4096, overlapPrc=75):
        self.semitonesShift = semitonesShift
        self.enablePeakPhaseLocking = enablePeakPhaseLocking
        self.enablePeakTracking = enablePeakTracking
        self.fftFrameLength = fftFrameLength                    # should be 4096 for pitch shifting
        self.overlapPrc = overlapPrc                            # should be 75% for pitch shifting

# pitchShiftFactor defines the pitch shift by octaves factor=hopSamplesOut/hopSamplesIn (eg. on octave up = 2.0, on octave down = 0.5)
class PhaseVocoderPitchShift(PhaseVocoder):
    def __init__(self, params: PhaseVocoderPitchShiftParams, pitchShiftFactor=2.0):
        self.params = params
        self.pitchShiftFactor = pitchShiftFactor
        self.hopSamples = int(params.fftFrameLength * (1 - params.overlapPrc / 100))
        self.hopSamplesOut = self.hopSamples * pitchShiftFactor
        self.phase = np.array(params.fftFrameLength)
        self.adjustedPhase = np.array(params.fftFrameLength)
        self.peakData = np.array(self.frameLength, dtype=FrequencyPeakInformation)
        self.prevPeakData = np.array(self.frameLength, dtype=FrequencyPeakInformation)
        self.peakIndex = np.array(self.frameLength, dtype=bool)
        self.prevPeakIndex = np.array(self.frameLength, dtype=bool)
        self.outBufferSize = self.frameLength
        super().__init__(frameLength=params.fftFrameLength, overlapPerc=params.overlapPrc)

    def _preparePitchShift(self, semitones: float):
        pitchShiftFactor = pow(2.0, semitones / 12.0)       # 1 octave = 12 semitones
        newOutBufferSize = round((1.0 / pitchShiftFactor) * self.frameLength)

        # check if smth changed
        if newOutBufferSize == self.outBufferSize:
            return
        
        # set changed values
        self.pitchShiftFactor = pitchShiftFactor
        self.hopSamples = self.hopSamplesOut / self.pitchShiftFactor
        self.outBufferSize = newOutBufferSize
        self.window = getHannWindow(self.outBufferSize)
        self.normWindowGain = self._getNormWindowGain(self.window, 1) # todo: overlapPc needed?
        
        # create new outputbuffer
        self.outputBuffer = np.zeros(self.outBufferSize)

    def applyEffect(self, fftValues):
        # a new frame is rdy for work
        # first check if peak phase locking has to be done
        if self.params.enablePeakPhaseLocking == True:
            # do stuff
            pass


        else:
            # do without phase locking
            pass

        # than we need the ifft output, because more stuff is to do
        # maybe redo processHop, because we may have to overwrite the output, or use another output buffer for returning y

        # do resampling because of stretching

        # overlap and add the output

        

