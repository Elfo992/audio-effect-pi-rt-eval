from tools.helper import bipolarToUnipolar, unipolarModulation, bipolarModulation, nearestPowerOf2
from tools.buffer import *
from oscillators.oscillators import *

class Effect:
    def applyEffect(self, x: float) -> float:
        pass

class PassThrough(Effect):
    def applyEffect(self, x: float) -> float:
        return x
    
class DigitalDelayParameters(): # -3db = 0.707
    def __init__(self, delayMs, wetGainDb=-3.0, dryGainDb=-3.0, feedbackPrc=50.0, bufferLengthMs=2000):
        self.delayMs = delayMs
        self.wetGainDb = wetGainDb
        self.dryGainDb = dryGainDb
        self.feedbackPrc = feedbackPrc
        self.bufferLengthMs = bufferLengthMs

class DigitalDelay(Effect):
    def __init__(self, sampleRate, params: DigitalDelayParameters):
        self.sampleRate = sampleRate
        self.params = params
        self.wetGain = pow(10.0, self.params.wetGainDb / 20.0)
        self.dryGain = pow(10.0, self.params.dryGainDb / 20.0)
        self.samplesMs = sampleRate / 1000.0
        self.feedback = params.feedbackPrc / 100.0
        self.delaySamples = self.params.delayMs * self.samplesMs
        self.buffer = self._initDelayBuffer(params.bufferLengthMs)

    def _initDelayBuffer(self, bufferLengthMs) -> CircularBuffer:
        bufferLength = (bufferLengthMs * self.samplesMs) + 1
        
        # we always want length by the power of 2
        bufferLength = nearestPowerOf2(bufferLength)
        return CircularBuffer(bufferLength)
    
    def applyEffect(self, x: float) -> float:
        # get the delayed output sample
        y = self.buffer.getLeft(self.delaySamples)
        
        # add new element to buffer (dont forget feedback)
        toAdd = x + (self.feedback) * y
        self.buffer.append(toAdd)

        # removed dryGain for now, because of calc time
        return self.wetGain * y
    
    # todo: check if the buffer has to be re init, because size changes
    def changeDelay(self, delayMs):
        self.delaySamples = delayMs * self.samplesMs


class ModulatedDelayParameters():
    def __init__(self, oscFreq, oscDepth, feedbackPrc=50.0, wetToDryPercentage=50):
        self.oscFreq = oscFreq          # freq of the osc (use lowFreqOsc if possible)
        self.oscDepth = oscDepth        # how much of the available delay time range is used
        self.feedbackPrc = feedbackPrc  
        self.wetToDryPercentage = wetToDryPercentage # 100% wet = vibrato, 50/50 = flanger


# Pirkle S. 420 Table 15.1
'''
        min-delay ms    mod-depth ms    wet-mix dB  dry-mix dB  feedback %  
Flanger 1               2-7             -3          -3          -99 to +99
Vibrato 0               3-7             0           -96 (off)   0 
Chorus  1-30            1-30            -3          0           0
White-  1-30            1-30            -3          0           -70
Chorus
'''
class ModulatedDelay(Effect):
    def __init__(self, sampleRate, minDelayMs, maxModDepthMs, paramsMD: ModulatedDelayParameters, paramsDD: DigitalDelayParameters):
        self.params = paramsMD
        self.minDelayMs = minDelayMs
        self.maxModDepthMs = maxModDepthMs
        self.osc = LowFrequencySineOscillator(sampleRate, paramsMD.oscFreq)   # todo: check sample rate, how does it effect the effect xD
        self.digDelay = DigitalDelay(sampleRate, paramsDD)
        self.digDelay.params.feedbackPrc = paramsMD.feedbackPrc         # has to have the same feedback, set in ctor to be sure
        self.oscDepth = self.params.oscDepth * 0.01

    def applyEffect(self, x: float) -> float:
        pass

# todo: remove code redundancies in applyModulatedDelay, only modulation is different
class ModulatedDelayFlanger(ModulatedDelay):
    MIN_DELAY_MS = 1.0
    MAX_MOD_DEPTH_MS = 7.0

    def __init__(self, sampleRate, paramsMD: ModulatedDelayParameters):
        # info: delay is set by the osc
        paramsDD = DigitalDelayParameters(delayMs=10.0, wetGainDb=-3.0, dryGainDb=-3.0)  
        return super().__init__(sampleRate, self.MIN_DELAY_MS, self.MAX_MOD_DEPTH_MS, paramsMD, paramsDD)
    
    def applyEffect(self, x: float) -> float:
        modMaxValue = self.minDelayMs + self.maxModDepthMs

        # do modulation and set new delay time, flanger is unipolar
        convertedToUni = bipolarToUnipolar(self.oscDepth * self.osc.getOscillator().oscOutput)
        modValue = unipolarModulation(toModValue=convertedToUni, minValue=self.minDelayMs, maxValue=modMaxValue)
        self.digDelay.changeDelay(modValue)

        # apply new delay
        return self.digDelay.applyEffect(x)
    
class ModulatedDelayChorus(ModulatedDelay):
    MIN_DELAY_MS = 10.0
    MAX_MOD_DEPTH_MS = 30.0
    
    def __init__(self, sampleRate, paramsMD: ModulatedDelayParameters):
        # info: delay is set by the osc
        paramsDD = DigitalDelayParameters(delayMs=1.0, wetGainDb=-3.0, dryGainDb=0.0, feedbackPrc=0)
        return super().__init__(sampleRate, self.MIN_DELAY_MS, self.MAX_MOD_DEPTH_MS, paramsMD, paramsDD)
    
    def applyEffect(self, x: float) -> float:
        modMaxValue = self.minDelayMs + self.maxModDepthMs

        # do modulation and set new delay time, chorus is bipolar
        toModValue = self.oscDepth * self.osc.getOscillator().oscOutput
        modValue =  bipolarModulation(toModValue, self.minDelayMs, modMaxValue)
        self.digDelay.changeDelay(modValue)

        # apply new delay
        return self.digDelay.applyEffect(x)
    
class ModulatedDelayVibrato(ModulatedDelay):
    MIN_DELAY_MS = 0.0
    MAX_MOD_DEPTH_MS = 7.0
    
    def __init__(self, sampleRate, paramsMD: ModulatedDelayParameters):
        # info: delay is set by the osc
        paramsDD = DigitalDelayParameters(delayMs=1.0, wetGainDb=0.0, dryGainDb=-96.0, feedbackPrc=0)
        return super().__init__(sampleRate, self.MIN_DELAY_MS, self.MAX_MOD_DEPTH_MS, paramsMD, paramsDD)
    
    def applyEffect(self, x: float) -> float:
        modMaxValue = self.minDelayMs + self.maxModDepthMs

        # do modulation and set new delay time, vibrato is bipolar
        toModValue = self.oscDepth * self.osc.getOscillator().oscOutput
        modValue =  bipolarModulation(toModValue, self.minDelayMs, modMaxValue)
        self.digDelay.changeDelay(modValue)

        # apply new delay
        return self.digDelay.applyEffect(x)