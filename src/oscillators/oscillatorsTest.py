import matplotlib.pyplot as plt
import numpy as np

from oscillators.oscillators import *

fig, axs = plt.subplots(3)
times = np.arange(500)

osc = LowFrequencySineOscillator(48000, 1000)
output = np.zeros(500)
for i in range(500):
    output[i] = osc.getOscillator().oscOutput
axs[2].plot(times, np.zeros(500), linewidth=0.8, color='0')
axs[2].plot(times, output)
axs[2].set_title('Sinus-Oszillator')
axs[2].set_ylabel('Amplitude')
axs[2].set_xlabel('Zeit')
axs[2].set_xlim(0, 500)

osc = LowFrequencyTriangleOscillator(48000, 1000)
output = np.zeros(500)
for i in range(500):
    output[i] = osc.getOscillator().oscOutput
axs[1].plot(times, np.zeros(500), linewidth=0.8, color='0')
axs[1].plot(times, output)
axs[1].set_title('Dreieck-Oszillator')
axs[1].set_ylabel('Amplitude')
axs[1].set_xlabel('Zeit')
axs[1].set_xlim(0, 500)

osc = LowFrequencySawtoothOscillator(48000, 1000)
output = np.zeros(500)
for i in range(500):
    output[i] = osc.getOscillator().oscOutput
axs[0].plot(times, np.zeros(500), linewidth=0.8, color='0')
axs[0].plot(times, output)
axs[0].set_title('Sägezahn-Oszillator')
axs[0].set_ylabel('Amplitude')
axs[0].set_xlabel('Zeiin s')
axs[0].set_xlim(0, 500)

plt.grid(visible=True, which='minor')
plt.subplots_adjust(hspace=1, top=0.9)
plt.show()
