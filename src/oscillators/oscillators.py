from scipy import signal
import numpy as np
from typing import NamedTuple

class OscillatorOutputData(NamedTuple):
    oscOutput: float
    oscQuadOutput: float        # 90° shifted output
    oscNegOutput: float         # inverted output
    oscQuadNegOutput: float     # 90° shifted and inverted output

# Pirkle S. 355. Oscillator with a simple modulo counter
class LowFrequencyOscillator(): 
    
    QUAD_COUNT = 0.25 # 90° phase shift (1/4 of max value (1))
    def __init__(self, sampleRate, oscFreq):
        self.oscFreq = oscFreq
        self.phaseIncrement = oscFreq / sampleRate # step size for increment
        self.modCounter = 0.0
        self.modCounterQuad = self.QUAD_COUNT 
    
    # inc is the current phase
    def getOscillator(self) -> OscillatorOutputData:
        pass

    def _initModCounter(self):
        # check the main modulo counter for overflow
        self.modCounter = self._checkModuloCounter(self.modCounter, self.phaseIncrement)

        # reset quad modulo counter
        self.modCounterQuad = self.modCounter

        # increment quad counter by 90°
        self.modCounterQuad += self.QUAD_COUNT

        # check the quad counter for overflow
        self.modCounterQuad= self._checkModuloCounter(self.modCounterQuad, self.QUAD_COUNT)

    def _incrementModCounter(self):
        # increment modulo counter
        self.modCounter += self.phaseIncrement
    
    # check if modulo has reached max and reset
    def _checkModuloCounter(self, modCounter, phaseIncrement):
        # positive phase check
        if phaseIncrement > 0.0 and modCounter >= 1.0:
            modCounter = 0.0
        
        # negative phase check
        if phaseIncrement < 0.0 and modCounter <= 0.0:
            modCounter = 0.0
        
        return modCounter

    # fast sine calc for angle [-pi pi]
    # https://github.com/kennyalive/fast-sine-cosine/blob/master/src/main.cpp
    B = 4.0 / np.pi
    C = -4.0 / (np.pi * np.pi)
    P = 0.225
    def _fastSine(self, angle: float):
        y = self.B * angle + self.C * angle * abs(angle)
        y = self.P * (y * abs(y) - y) + y
        return y

class LowFrequencySineOscillator(LowFrequencyOscillator):
    def __init__(self, sampleRate, oscFreq):
        super().__init__(sampleRate, oscFreq)

    def getOscillator(self) -> OscillatorOutputData:
        self._initModCounter()

        # create sine osc
        # calc angle, keep in min only -pi to pi
        angle = self.modCounter * 2 * np.pi - np.pi
        angleQuad = self.modCounterQuad * 2 * np.pi - np.pi
        
        # calc fast sine
        oscOutput = self._fastSine(-angle)
        oscQuadOutput = self._fastSine(-angleQuad)

        self._incrementModCounter()
        return OscillatorOutputData(oscOutput, oscQuadOutput, -oscOutput, -oscQuadOutput)

class LowFrequencyTriangleOscillator(LowFrequencyOscillator):
    def __init__(self, sampleRate, oscFreq):
        super().__init__(sampleRate, oscFreq)

    def getOscillator(self) -> OscillatorOutputData:
        self._initModCounter()

        # shift it onto the middle of the x axis (adds negative values)
        shiftedModCounter = self.modCounter * 2.0 - 1.0
        shiftedModCounterQuad = self.modCounterQuad * 2.0 - 1.0
        
        # create triangle osc
        oscOutput = 2.0 * abs(shiftedModCounter) - 1.0
        oscQuadOutput = 2.0 * abs(shiftedModCounterQuad) - 1.0
    
        self._incrementModCounter()
        return OscillatorOutputData(oscOutput, oscQuadOutput, -oscOutput, -oscQuadOutput)


class LowFrequencySawtoothOscillator(LowFrequencyOscillator):
    def __init__(self, sampleRate, oscFreq):
        super().__init__(sampleRate, oscFreq)

    def getOscillator(self) -> OscillatorOutputData:
        self._initModCounter()

        # shift it onto the middle of the x axis (adds negative values)
        oscOutput = self.modCounter * 2.0 - 1.0
        oscQuadOutput = self.modCounterQuad * 2.0 - 1.0

        self._incrementModCounter()
        return OscillatorOutputData(oscOutput, oscQuadOutput, -oscOutput, -oscQuadOutput)

class BasicOscillator:
    def __init__(self, oscFreq, oscGain, oscLength):
        self.oscFreq = oscFreq
        self.oscGain = oscGain
        self.oscLength = oscLength
        
    def multiplyOscillator(self, x: float, sampleFreq):
        pass

    def _getOscillator(self, sampleFreq):
        pass
    
class SineOscillator(BasicOscillator):
    def __init__(self, oscFreq, oscGain, oscLength):
        super().__init__(oscFreq, oscGain, oscLength)

    def multiplyOscillator(self, x: float, sampleFreq):
        sine = self._getOscillator(sampleFreq)
        return x * self.oscGain * sine
    
    def _getOscillator(self, sampleFreq):
        # inputVec = np.arange(self.oscLength)
        return np.expand_dims(np.sin(2 * np.pi * self.oscFreq * self.oscLength / sampleFreq), axis=1)
    
class SawtoothOscillator(BasicOscillator):
    def __init__(self, oscFreq, oscGain, oscLength):
        super().__init__(oscFreq, oscGain, oscLength)

    def multiplyOscillator(self, x: float, sampleFreq):
        sine = self._getOscillator(sampleFreq)
        return x * self.oscGain * sine
    
    def _getOscillator(self, sampleFreq):
        # inputVec = np.zeros(self.oscLength) # todo: check 
        return np.expand_dims(signal.sawtooth(2 * np.pi * self.oscFreq * self.oscLength / sampleFreq), axis=1)