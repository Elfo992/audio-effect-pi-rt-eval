## Oscillators
This folder contains the implementation of low frequency oscillators, used for several effects. 
Three different waveforms (sawtooth, triangle, sine) can be generated.