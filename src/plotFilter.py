import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

from filters.filters import *
from filters.filterCoeffs import *

# Define filter
lpFilter = BiquadLP2Filter(BiquadFilterCoeffCalculatorParams(48000, fc=5000, wetToDryPercentage=90))
hpFilter = BiquadHP2Filter(BiquadFilterCoeffCalculatorParams(48000, fc=5000, wetToDryPercentage=90))
notchFilter = BiquadNotchFilter(BiquadFilterCoeffCalculatorParams(48000, fc=1500, wetToDryPercentage=90))

# Define the transfer function coefficients
a = [1]  # Numerator coefficients
b = [1, 1/0.7, 1]  # Denominator coefficients

# Create a transfer function using scipy.signal.TransferFunction
sys = signal.TransferFunction(a, b)

# Generate the frequency response
w, mag, phase = signal.bode(sys)
frequencies = w / (2 * np.pi)

# Create the Bode plot
plt.figure(figsize=(10, 6))

# Magnitude plot
plt.subplot(2, 1, 1)
plt.semilogx(frequencies, mag)
plt.grid()
plt.ylabel('Magnitude (dB)')
plt.title('Bode Diagram - Biquad Low-Pass Filter')

# Phase plot
plt.subplot(2, 1, 2)
plt.semilogx(frequencies, phase)
plt.grid()
plt.ylabel('Phase (degrees)')
plt.xlabel('Frequency (Hz)')

plt.tight_layout()
plt.show()