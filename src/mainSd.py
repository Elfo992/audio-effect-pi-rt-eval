import sounddevice as sd # Soundcard audio I/O access library
import numpy as np
import matplotlib.pyplot as plt
from time import perf_counter

from filters.filters import *
from effects.effects import *
from effects.filterEffects import *
from effects.phaseShifter import *
from effects.vocoder import *
from processors.processors import *

# for logging
import sys
from datetime import datetime

# uncomment to write console output to log file
# sys.stdout = open("output.log", 'w')
sys.stderr = sys.stdout

# Setup channel info
FORMAT = np.int16           # data type formate
CHANNELS = 1                # Adjust to your number of channels
RATE = 48000                # sample Rate
CHUNK = 2**10               # block Size
INPUT_INDEX = 1             # input device index (use getInputDevice.py)
OUTPUT_INDEX = 3            # index of the output device

# create filter, effect, oscillator for processor
digitalDelay = DigitalDelay(RATE, DigitalDelayParameters(delayMs=1000))
flanger = ModulatedDelayFlanger(RATE, ModulatedDelayParameters(10, 7.0))
chorus = ModulatedDelayChorus(RATE, ModulatedDelayParameters(0.5, 30, feedbackPrc=0.0))
vibrato = ModulatedDelayVibrato(RATE, ModulatedDelayParameters(50, 7.0, feedbackPrc=0.0))
robotVocoder = PhaseVocoderRobot(512, 25)
whisperVocoder = PhaseVocoderWhisper(512, 25)
filterEffect = FilterEffect(RATE)

# too slow
phaseShifter = PhaseShifter(RATE, 5)

# create processor
audioProcessor = AudioProcessor(RATE)
effectId = 1

def initProcessor():
    audioProcessor.addEffect(digitalDelay)
    audioProcessor.addEffect(flanger)
    audioProcessor.addEffect(chorus)
    audioProcessor.addEffect(vibrato)
    audioProcessor.addEffect(robotVocoder)
    audioProcessor.addEffect(whisperVocoder)
    audioProcessor.addEffect(filterEffect)

def callback(indata, outdata, frames, time, status):
    # normalize data (prikle s.6) 16 bit
    normData = indata / 2**16

    # process and measure time
    start = perf_counter()
    effectedData = audioProcessor.processAudio(normData)
    end = perf_counter()
    print("effect=%d, elapsedTime=%f" % (effectId, end-start))
    
    # denormalize data 
    denormData = effectedData * 2**15
    outdata[:] = denormData

# print all and selected I/O devices   
print(sd.query_devices())
print("input dev index: " + str(INPUT_INDEX) + "\noutput dev index: " + str(OUTPUT_INDEX))

# add effects to processor
initProcessor()

# set hardcoded effect for testing
audioProcessor.changeEffect(effectId)

# start stream and call callback with effect if buffer is full
with sd.Stream(device=(INPUT_INDEX, OUTPUT_INDEX),
        samplerate= RATE, blocksize=CHUNK,
        dtype=FORMAT, channels=CHANNELS, callback=callback):
    
    while(1):
        # if sleep is added, the sound is better
        sd.sleep(1)
