import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

# # LP2
# # Filter specifications
# cutoff_freq = 500  # Cutoff frequency in Hz
# Q_values = [0.707, 10, 100]  # List of quality factors

# # Frequency range for plotting
# frequencies = np.logspace(0, 5, 1000)  # Frequencies from 1 Hz to 10 kHz

# plt.figure(figsize=(10, 8))

# for Q in Q_values:
#     # Create the transfer function
#     numerator = [cutoff_freq**2]
#     denominator = [1, cutoff_freq/Q, cutoff_freq**2]
#     sys = signal.TransferFunction(numerator, denominator)

#     # Calculate frequency response
#     w, mag, phase = signal.bode(sys, frequencies)

#     # Magnitude plot
#     plt.subplot(2, 1, 1)
#     plt.semilogx(frequencies, mag, label=f'Q = {Q}')
#     plt.grid()
#     plt.ylabel('Amplitude in dB', fontsize = 13.0)
#     plt.title('Tiefpass 2. Ordnung', fontsize = 16.0)
#     plt.legend()

#     # Phase plot
#     plt.subplot(2, 1, 2)
#     plt.semilogx(frequencies, phase, label=f'Q = {Q}')
#     plt.grid()
#     plt.ylabel('Phase in °', fontsize = 13.0)
#     plt.xlabel('Frequenz in Hz', fontsize = 13.0)
#     plt.legend()

# # HP2
# # Filter specifications
# cutoff_freq = 500  # Cutoff frequency in Hz
# Q_values = [0.707, 10, 100]  # List of quality factors

# # Frequency range for plotting
# frequencies = np.logspace(0, 5, 1000)  # Frequencies from 1 Hz to 10 kHz

# plt.figure(figsize=(10, 8))

# for Q in Q_values:
#     # Create the transfer function for highpass filter
#     numerator = [1, 0, 0]  # Highpass filter numerator coefficients
#     denominator = [1, cutoff_freq/Q, cutoff_freq**2]  # Highpass filter denominator coefficients
#     sys = signal.TransferFunction(numerator, denominator)

#     # Calculate frequency response
#     w, mag, phase = signal.bode(sys, frequencies)

#     # Magnitude plot
#     plt.subplot(2, 1, 1)
#     plt.semilogx(frequencies, mag, label=f'Q = {Q}')
#     plt.grid()
#     plt.ylabel('Amplitude in dB', fontsize = 13.0)
#     plt.title('Hochpass 2. Ordnung', fontsize = 16.0)
#     plt.legend()

#     # Phase plot
#     plt.subplot(2, 1, 2)
#     plt.semilogx(frequencies, phase, label=f'Q = {Q}')
#     plt.grid()
#     plt.ylabel('Phase in °', fontsize = 13.0)
#     plt.xlabel('Frequenz in Hz', fontsize = 13.0)
#     plt.legend()

# # BP2
# # Filter specifications
# center_freq = 500  # Center frequency in Hz
# Q_values = [0.2, 10, 100]  # Quality factors

# # Frequency range for the plot (logarithmic scale)
# freq = np.logspace(0, 6, num=1000)  # 10^0 to 10^6 Hz

# plt.figure(figsize=(12, 8))

# for Q in Q_values:
#     # Calculate the angular frequency (in rad/s) based on the center frequency and Q
#     omega_0 = 2 * np.pi * center_freq
#     numerator = [0, omega_0 / Q, 0]
#     denominator = [1, omega_0 / Q, omega_0**2]

#     # Create a TransferFunction instance
#     system = signal.TransferFunction(numerator, denominator)

#     # Compute the magnitude and phase of the system
#     omega, mag, phase = signal.bode(system, freq)

#     # Plot Magnitude
#     plt.subplot(2, 1, 1)
#     plt.semilogx(omega, mag, label=f'Q = {Q}')

#     # Plot Phase
#     plt.subplot(2, 1, 2)
#     plt.semilogx(omega, phase, label=f'Q = {Q}')

# # Set plot labels and title for Magnitude plot
# plt.subplot(2, 1, 1)
# plt.title('Bandpass 2. Ordnung', fontsize = 16.0)
# plt.ylabel('Amplitude in dB', fontsize = 13.0)
# plt.grid()
# plt.legend()

# # Set plot labels and title for Phase plot
# plt.subplot(2, 1, 2)
# plt.xlabel('Frequenz in Hz', fontsize = 13.0)
# plt.ylabel('Phase in °', fontsize = 13.0)
# plt.grid()
# plt.legend()


# # Notch
# # Filter specifications
# center_freq = 500  # Center frequency in Hz
# Q_values = [0.2, 2, 5]  # Quality factors

# # Frequency range for the plot (logarithmic scale)
# freq = np.logspace(0, 6, num=1000)  # 10^0 to 10^6 Hz

# plt.figure(figsize=(12, 8))

# for Q in Q_values:
#     # Calculate the angular frequency (in rad/s) based on the center frequency and Q
#     omega_0 = 2 * np.pi * center_freq
#     numerator = [1, 0, omega_0**2]
#     denominator = [1, omega_0 / Q, omega_0**2]

#     # Create a TransferFunction instance
#     system = signal.TransferFunction(numerator, denominator)

#     # Compute the magnitude and phase of the system
#     omega, mag, phase = signal.bode(system, freq)

#     # Plot Magnitude
#     plt.subplot(2, 1, 1)
#     plt.semilogx(omega, mag, label=f'Q = {Q}')

#     # Plot Phase
#     plt.subplot(2, 1, 2)
#     plt.semilogx(omega, phase, label=f'Q = {Q}')

# # Set plot labels and title for Magnitude plot
# plt.subplot(2, 1, 1)
# plt.title('Notch', fontsize = 16.0)
# plt.ylabel('Amplitude in dB', fontsize = 13.0)
# plt.grid()
# plt.legend()

# # Set plot labels and title for Phase plot
# plt.subplot(2, 1, 2)
# plt.xlabel('Frequenz in Hz', fontsize = 13.0)
# plt.ylabel('Phase in °', fontsize = 13.0)
# plt.grid()
# plt.legend()


# Allpass
# Parameters
Q = 0.707
f = 100  # Frequency in Hz
omega_n = 2 * np.pi * f
alpha = 1 / (2 * Q)

# Transfer function coefficients
numerator = [1, -2 * alpha * omega_n, omega_n ** 2]
denominator = [omega_n ** 2, -2 * alpha * omega_n, 1]

# Create the transfer function
system = signal.TransferFunction(numerator, denominator)

# Frequency range for plotting
frequencies = np.logspace(0, 8, num=1000)  # From 10 to 100 Hz

# Calculate the phase response
w, mag, phase = signal.bode(system, frequencies)

# Plot the phase response
plt.semilogx(w, phase)
plt.xlabel('Frequency [rad/s]')
plt.ylabel('Phase [degrees]')
plt.title('Phase Response of Second-Order Allpass Filter')
plt.grid(True)

plt.tight_layout()
plt.show()