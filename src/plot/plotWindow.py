import matplotlib.pyplot as plt
from scipy import signal
import numpy as np

fs = 1000
t = np.arange(0, 4, 1/fs)
f = 1
x = np.sin(2 * np.pi * f * t)

w = signal.windows.hann(int(len(t)/2), False)
wPad = np.pad(w, 1000)

windowed = x*wPad

plt.figure()
plt.plot(t, x)
plt.xlim(0, 4)
plt.axis('off')

plt.figure()
plt.plot(t, wPad)
plt.xlim(0, 4)
plt.axis('off')

plt.figure()
plt.plot(t, windowed)
plt.axis('off')
plt.xlim(0, 4)


plt.show()