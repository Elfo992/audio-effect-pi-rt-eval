import numpy as np
import wave
import matplotlib.pyplot as plt
from scipy import signal
from mpl_toolkits.mplot3d import Axes3D

def plotAudio():
    # read .wav file and get wave_read obj (rb)
    wav_obj_low = wave.open("pulasting-bass.wav", 'rb')
    wav_obj_high = wave.open("high_shooting_star.wav", 'rb')

    # get sample freq
    sample_freq_low = wav_obj_low.getframerate()
    sample_freq_high = wav_obj_high.getframerate()

    # get number of samples
    n_samples_low = wav_obj_low.getnframes()
    n_samples_high = wav_obj_high.getnframes()

    # read amplitudes (as bytes obj) for number of samples (reduce if lesser samples needed)
    signal_wave_low = wav_obj_low.readframes(n_samples_low)
    signal_wave_high = wav_obj_high.readframes(n_samples_high)

    # get actual values
    signal_array_low = np.frombuffer(signal_wave_low, dtype=np.int16)
    signal_array_high = np.frombuffer(signal_wave_high, dtype=np.int16)

    # convert to two separate channels (stereo)
    l_channel_low = signal_array_low[0::2]
    r_channel_low = signal_array_low[1::2]
    l_channel_high = signal_array_high[0::2]
    r_channel_high = signal_array_high[1::2]

    plot_time = 1

    # do fft
    X_l_low = np.fft.fft(l_channel_low[0:plot_time*sample_freq_low])
    N_l_low = len(X_l_low)
    n_l_low = np.arange(N_l_low)
    T_low = N_l_low/sample_freq_low
    freq_low = n_l_low/T_low

    X_l_high = np.fft.fft(l_channel_high[0:plot_time*sample_freq_high])
    N_l_high = len(X_l_high)
    n_l_high = np.arange(N_l_high)
    T_high = N_l_high/sample_freq_high
    freq_high = n_l_high/T_high

    # calc timestamps for plotting
    times_low = np.linspace(0, plot_time, num=plot_time*sample_freq_low)
    times_high = np.linspace(0, plot_time, num=plot_time*sample_freq_high)

    # time plot
    fig, axs = plt.subplots(2)

    # Subplot 1
    axs[0].plot(times_low, l_channel_low[0:plot_time*sample_freq_low])
    axs[0].set_title('niedrige Frequenzanteile')
    axs[0].set_ylabel('Amplitude', fontsize = 11.0)
    axs[0].set_xlabel('Zeit in s', fontsize = 11.0)
    axs[0].set_xlim(0, plot_time)
    axs[0].set_ylim(-45000, 45000)
    plt.rc('xtick', labelsize=11.0) 
    plt.rc('ytick', labelsize=11.0) 

    # Subplot 2
    axs[1].plot(times_high, l_channel_high[0:plot_time*sample_freq_high])
    axs[1].set_title('hohe Frequenzanteile')
    axs[1].set_ylabel('Amplitude', fontsize = 11.0)
    axs[1].set_xlabel('Zeit in s', fontsize = 11.0)
    axs[1].set_xlim(0, plot_time)
    axs[1].set_ylim(-45000, 45000)
    plt.suptitle('Darstellung Zeitbereich', y=1.05)
    plt.subplots_adjust(hspace=0.5, top=0.9)
    # plt.show()

    # freq spectrum
    fig, axs = plt.subplots(2)

    # Subplot 1
    axs[0].specgram(l_channel_low[0:plot_time*sample_freq_low],
                    Fs=sample_freq_low, vmin=-20, vmax=50)
    axs[0].set_title('niedrige Frequenzanteile')
    axs[0].set_ylabel('Frequenz in Hz', fontsize = 11.0)
    axs[0].set_xlabel('Zeit in s', fontsize = 11.0)
    axs[0].set_xlim(0, plot_time)
    fig.colorbar(axs[0].images[0], ax=axs[0], orientation='vertical')

    # Subplot 2
    axs[1].specgram(l_channel_high[0:plot_time*sample_freq_high],
                    Fs=sample_freq_high, vmin=-20, vmax=50)
    axs[1].set_title('hohe Frequenzanteile')
    axs[1].set_ylabel('Frequenz in Hz', fontsize = 11.0)
    axs[1].set_xlabel('Zeit in s', fontsize = 11.0)
    axs[1].set_xlim(0, plot_time)
    fig.colorbar(axs[1].images[0], ax=axs[1], orientation='vertical')

    plt.suptitle('Darstellung Spektogramm', y=1.05)
    plt.subplots_adjust(hspace=0.5, top=0.9)
    # plt.show()


    # fft
    fig, axs = plt.subplots(2)

    # Subplot 1
    axs[0].stem(freq_low, np.abs(X_l_low), 'b', markerfmt=" ", basefmt="-b")
    axs[0].set_title('niedrige Frequenzanteile')
    axs[0].set_xlabel('Frequenz in Hz', fontsize = 11.0)
    axs[0].set_ylabel('Amplitude', fontsize = 11.0)
    axs[0].set_xlim(0, 20000)

    # Subplot 2
    axs[1].stem(freq_high, np.abs(X_l_high), 'b', markerfmt=" ", basefmt="-b")
    axs[1].set_title('hohe Frequenzanteile')
    axs[1].set_xlabel('Frequenz in Hz', fontsize = 11.0)
    axs[1].set_ylabel('Amplitude', fontsize = 11.0)
    axs[1].set_xlim(0, 20000)

    plt.suptitle('Darstellung Frequenzbereich', y=1.05)
    plt.subplots_adjust(hspace=0.5, top=0.9)
    # plt.show()

    # Compute the spectrogram of the signal
    frequencies_low, times_low, Sxx_low = signal.spectrogram(l_channel_low, sample_freq_low)
    frequencies_high, times_high, Sxx_high = signal.spectrogram(l_channel_high, sample_freq_high)
    
    # Plot the 3D spectrogram
    fig = plt.figure()

    # Subplot 1
    ax1 = fig.add_subplot(121, projection='3d')
    T_low, F = np.meshgrid(times_low, frequencies_low)
    ax1.plot_surface(T_low, F, 10*np.log10(Sxx_low), cmap='jet')
    ax1.set_title('niedrige Frequenzanteile')
    ax1.set_xlabel('Zeit in s', fontsize = 11.0)
    ax1.set_ylabel('Frequenz in Hz', fontsize = 11.0)
    ax1.set_zlabel('Amplitude', fontsize = 11.0)

    # Subplot 2
    ax2 = fig.add_subplot(122, projection='3d')
    T_high, F_high = np.meshgrid(times_high, frequencies_high)
    ax2.plot_surface(T_high, F_high, 10*np.log10(Sxx_high), cmap='jet')
    ax2.set_title('hohe Frequenzanteile')
    ax2.set_xlabel('Zeit in s', fontsize = 11.0)
    ax2.set_ylabel('Frequenz in Hz', fontsize = 11.0)
    ax2.set_zlabel('Amplitude', fontsize = 11.0)

    plt.suptitle('Darstellung 3D', y=1.05)
    plt.subplots_adjust(hspace=0.5, top=0.9)
    plt.show()



plotAudio()