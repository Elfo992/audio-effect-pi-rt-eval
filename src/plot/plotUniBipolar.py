import matplotlib.pyplot as plt
from scipy import signal
import numpy as np

fs = 100
t = np.arange(0, 5, 1/fs)
f = 1
x = np.sin(2 * np.pi * f * t)

fig, axs = plt.subplots(2)
plt.rc('xtick', labelsize=11.0) 
plt.rc('ytick', labelsize=11.0) 
fig.tight_layout(pad=3)

axs[0].axhline(y=0, linewidth=0.5, color='black')
axs[0].axhline(y=-1, linewidth=0.5, color='black', linestyle='dashed')
axs[0].axhline(y=1, linewidth=0.5, color='black', linestyle='dashed')
axs[0].text(0, -1, "10 Hz   ", color="black", ha="right", va="center")
axs[0].text(0, 0, "11030 Hz   ", color="black", ha="right", va="center")
axs[0].text(0, 1, "22050 Hz   ", color="black", ha="right", va="center")
axs[0].plot(t, x)
axs[0].set_xlim(0,5)
axs[0].set_ylim(-1.2,1.2)
axs[0].set_title('bipolar')
# axs[0].set_ylabel('Amplitude')
axs[0].set_xlabel('Zeit')
axs[0].tick_params('y', labelleft=False)
axs[0].tick_params('x', labelbottom=False)

x = 0.5 * x + 0.5
axs[1].axhline(y=0, linewidth=0.5, color='black')
axs[1].axhline(y=0.5, linewidth=0.5, color='black', linestyle='dashed')
axs[1].axhline(y=1, linewidth=0.5, color='black', linestyle='dashed')

axs[1].text(0, 0, "10 Hz   ", color="black", ha="right", va="center")
axs[1].text(0, 0.5, "11030 Hz   ", color="black", ha="right", va="center")
axs[1].text(0, 1, "22050 Hz   ", color="black", ha="right", va="center")
axs[1].plot(t, x)
axs[1].set_xlim(0,5)
axs[1].set_ylim(-1.2,1.2)
axs[1].set_title('unipolar')
# axs[1].set_ylabel('Amplitude')
axs[1].set_xlabel('Zeit')
axs[1].tick_params('y', labelleft=False)
axs[1].tick_params('x', labelbottom=False)


plt.show()