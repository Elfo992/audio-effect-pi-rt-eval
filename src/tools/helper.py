import numpy as np
import wave
import math

def checkOverflow(y: float) -> float:
    if y > 0.0 and y < 1.175494351e-38:
        return 0.0
    elif y < 0.0 and y > -1.175494351e-38:
        return 0.0
    return y

def bipolarModulation(toModValue: float, minValue: float, maxValue: float) -> float:
    toModValue = min(toModValue, 1.0)
    toModValue = max(toModValue, -1.0)
    halfRange = (maxValue - minValue) / 2.0
    midpoint = halfRange + minValue
    return toModValue * halfRange + midpoint

def unipolarModulation(toModValue: float, minValue: float, maxValue: float) -> float:
    toModValue = min(toModValue, 1.0)
    toModValue = max(toModValue, 0.0)
    return toModValue * (maxValue - minValue) + minValue

def bipolarToUnipolar(value: float):
    return 0.5 * value + 0.5

def norm(x: float) -> float:
    return x  / 2**16

def nearestPowerOf2(n):
    # Find the power of 2 nearest to n
    exponent = round(math.log2(n))
    return int(math.pow(2, exponent))

def readWaveFile(filename):
    with wave.open(filename, 'r') as waveFile:
        sampleRate = waveFile.getframerate()
        numFrames = waveFile.getnframes()
        data = waveFile.readframes(numFrames)

    # Convert binary data to integers
    outputSignal = np.frombuffer(data, dtype=np.int16)
    outputSignal = outputSignal.astype(np.float32)
    return outputSignal, sampleRate, numFrames

def write_wave_file(filename, data, samplerate=44100, nchannels=1):
    # Open a wave file
    with wave.open(filename, 'w') as wav_file:
        # Set parameters for the wave file
        wav_file.setnchannels(nchannels)
        wav_file.setsampwidth(2)  # 2 bytes (16 bits) per sample
        wav_file.setframerate(samplerate)

        # Ensure the data is a numpy array and convert it to 16-bit data
        if not isinstance(data, np.ndarray):
            data = np.array(data)
        data = (data * np.iinfo(np.int16).max).astype(np.int16)

        # Write the data as a raw byte stream
        wav_file.writeframes(data.tobytes())
