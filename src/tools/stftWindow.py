from scipy import signal

def getHannWindow(windowSize) :
    return signal.windows.hann(windowSize, False)

def getHammingWindow(windowSize) :
    return signal.windows.hamming(windowSize, False)

def getBlackmanHarrisWindow(windowSize) :
    return signal.windows.blackmanharris(windowSize, False)

def getChebwinWindow(windowSize) :
    return signal.windows.chebwin(windowSize, False)
