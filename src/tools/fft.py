import numpy as np
import scipy.fft as fft

def applyFFT(input, frameLength):
    fftResult = fft.fft(input, n=frameLength)
    return fftResult

def applyIFFT(input, frameLength):
    ifftResult = fft.ifft(input, n=frameLength)
    return ifftResult
