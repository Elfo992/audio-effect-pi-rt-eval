## Tools and Helper
This folder contains tools and helper functions, which were used during the development of this thesis.

### Buffer
The file [buffer.py](buffer.py) contains the implementation of a circular Buffer for float values, which was used for storing sample for different effects.


### FFT
The file [fft.py](fft.py) contains FFT and IFFT functions, currently only mapping onto the *scipy.fft* package.

### Helper
The file [fft.py](fft.py) contains the following functions:
- ``bipolarModulation()``
    - modulates the given value between -1 and +1
- ``unipolarModulation()``
    - modulates the given value between 0 and +1
- ``bipolarToUnipolar()``
    - converts a bipolar value (-1 to +1) to an unipolar value 0 to +1
- ``nearestPowerOf2()``
    - returns the nearest power of 2 to the given value

### STFT Windows
The file [stftWindow.py](stftWindow.py) contains mappings to the window functions Hann, Hamming, BlackmannHarris and Chebwin of the *scipy.signal* package.