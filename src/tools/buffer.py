import numpy as np

class   CircularBuffer:
    def __init__(self, size: int):
        self.buffer = np.zeros(size, dtype=float)
        self.size = size
        self.end = 0
        self.isFull = False

    def append(self, item: float):
        self.buffer[self.end] = float(item)
        self.end = (self.end + 1) % self.size # modulo to do the rewrap n%mod(n)=0
        if self.end == 0:
            self.isFull = True

    def appendMultiple(self, items):
        for item in items:
            self.append(item)

    def appendByAdding(self, item: float):
        self.buffer[self.end] += float(item)
        self.end = (self.end + 1) % self.size
        if self.end == 0:
            self.isFull = True
    
    def appendMultipleByAdding(self, items):
        for item in items:
            self.appendByAdding(item)

    def get(self, index: int) -> float:
        if index < 0 or index >= self.size:
            raise ValueError("Index out of range")
        return float(self.buffer[int(index)])

    def getLeft(self, index: int):
        delayIndex = int((self.end - int(index)) % self.size)
        return self.buffer[delayIndex]
    
    def pop(self):
        item = self.buffer[self.end]
        self.buffer[self.end] = 0.0
        self.end = (self.end + 1) % self.size
        return float(item)
    
    def getAllRight(self):
        buffer = np.zeros(self.size, dtype=float)
        for n in range(self.size):
            index =  (self.end + n) % self.size
            buffer[n] = self.buffer[index]
        return buffer
    

def testAllRight():
    b = CircularBuffer(10)
    for n in range(0, 8):
        b.append(n)
    
    print(b.buffer)
    print(b.getAllRight())
