from effects.effects import *

class AudioProcessor():
    def __init__(self, sampleRate):
        self.sampleRate = sampleRate
        self.currentEffectIndex = 0
        self.effects = []
        self.currentEffect = PassThrough()
        self.addEffect(self.currentEffect)

    def addEffect(self, effect : Effect):
        self.effects.append(effect)

    def changeEffect(self, effectIndex):
        if effectIndex >= 0 and effectIndex < len(self.effects) :
            self.currentEffect = self.effects[effectIndex]
            self.currentEffectIndex = effectIndex
            return
        
        print("index out of range, no changes")

    def processAudio(self, audioData):
        processedData = []
        for n, data in enumerate(audioData):
            x = self.currentEffect.applyEffect(data[0])
            processedData.append(x)
        
        return np.expand_dims(processedData, axis=1)