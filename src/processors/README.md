## Audio Processor
The Audio Processor applies the selected effect onto every sample.

- [processor.py](processors.py)
    - default processor used by main-scripts
- [testProcessor.py](testProcessors.py)
    - was used for testing while development