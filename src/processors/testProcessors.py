from filters.filters import *
from effects.effects import *
from oscillators.oscillators import *
from effects.phaseShifter import PhaseShifter
from effects.vocoder import PhaseVocoder
from tools.helper import bipolarModulation

sineOsc = LowFrequencySineOscillator(10000, 50)
class TestProcessor():
    def __init__(self, sampleRate, phaseShifter: PhaseShifter, digitalDelay: DigitalDelay, modDelay: ModulatedDelay, vocoder: PhaseVocoder, filter: DirectBiquadFilter, effect: Effect):
        self.sampleRate = sampleRate
        self.phaseShifter = phaseShifter
        self.digitalDelay = digitalDelay
        self.modDelay = modDelay
        self.vocoder = vocoder
        self.filter = filter
        self.effect = effect

    def processAudio(self, audioData):
        processedData = np.zeros(len(audioData))
        processedData = []
        for n, data in enumerate(audioData):
            x = self._doEffect(data[0])
            processedData.append(x)
        
        return np.expand_dims(processedData, axis=1)
    
    def _doFilter(self, x: float) -> float:
        return self.filter.applyFilter(x)
    
    def _doPhaseShift(self, x: float) -> float:
        return self.phaseShifter.applyEffect(x)

    def _doGainOsc(self, x: float) -> float:
        mod = sineOsc.getOscillator().oscOutput
        mod = bipolarModulation(mod, -1.0, 1.0)
        return x * mod
    
    def _doDelay(self, x: float) -> float:
        return self.digitalDelay.applyEffect(x)
    
    def _doModDelay(self, x: float) -> float:
        return self.modDelay.applyEffect(x)
    
    def _doVocoder(self, x: float) -> float:
        return self.vocoder.applyEffect(x)
    
    def _doEffect(self, x: float) -> float:
        return self.effect.applyEffect(x)

