import sounddevice as sd # Soundcard audio I/O access library
import numpy as np
import matplotlib.pyplot as plt
from time import perf_counter
from pad4pi import rpi_gpio
import cProfile

from filters.filters import *
from effects.effects import *
from effects.filterEffects import *
from effects.phaseShifter import *
from effects.vocoder import *
from processors.processors import *

# for logging
import sys
from datetime import datetime

# uncomment to write console output to log file
# sys.stdout = open("output.log", 'w')
sys.stderr = sys.stdout

# Setup channel info
FORMAT = np.int16           # data type formate
CHANNELS = 1                # Adjust to your number of channels
RATE = 48000                # Sample Rate
CHUNK = 2**10               # Block Size
INPUT_INDEX = 1             # input device index (use getInputDevice.py)
OUTPUT_INDEX = 0            # index of the output device

# create filter, effect, oscillator for processor
digitalDelay = DigitalDelay(RATE, DigitalDelayParameters(delayMs=1000))
flanger = ModulatedDelayFlanger(RATE, ModulatedDelayParameters(10, 7.0))
chorus = ModulatedDelayChorus(RATE, ModulatedDelayParameters(0.5, 30, feedbackPrc=0.0))
vibrato = ModulatedDelayVibrato(RATE, ModulatedDelayParameters(50, 7.0, feedbackPrc=0.0))
robotVocoder = PhaseVocoderRobot(512, 25)
whisperVocoder = PhaseVocoderWhisper(512, 25)
filterEffect = FilterEffect(RATE)

# create processor
audioProcessor = AudioProcessor(RATE)

# init keypad
# HifiBerry uses:   GPIO2-3 (pins 3 and 5),
#                   GPIOs 18-21 (pins 12, 35, 38 and 40)
# Pi Pins for pad:  R[7, 8, 10, 11] C[22, 13, 15, 16]
ROW_PINS = [4, 14, 15, 17]
COL_PINS = [25, 27, 22, 23]
factory = rpi_gpio.KeypadFactory()
keypad = factory.create_keypad(row_pins=ROW_PINS, col_pins=COL_PINS)

def keypadCallback(key):
    keyInt = int(key)
    print(f"{key} = {keyInt}")
    if keyInt >= 0 and keyInt <= 9:
        audioProcessor.changeEffect(keyInt)

def initProcessor():
    audioProcessor.addEffect(digitalDelay)
    audioProcessor.addEffect(flanger)
    audioProcessor.addEffect(chorus)
    audioProcessor.addEffect(vibrato)
    audioProcessor.addEffect(robotVocoder)
    audioProcessor.addEffect(whisperVocoder)
    audioProcessor.addEffect(filterEffect)

def callback(indata, outdata, frames, time, status):
    # normalize input of 16-bit
    # denormalize output only by 2^14 -> better sound
    outdata[:] = audioProcessor.processAudio(indata / 2**16) * 2**14

# print all and selected I/O devices   
print(sd.query_devices())
print("input dev index: " + str(INPUT_INDEX) + "\noutput dev index: " + str(OUTPUT_INDEX))

# add effects to processor
initProcessor()

# register callback func for keypad
keypad.registerKeyPressHandler(keypadCallback)

def run():
    # start stream and call callback with effect if buffer is full
    with sd.Stream(device=(INPUT_INDEX, OUTPUT_INDEX),
            samplerate=RATE, blocksize=CHUNK, dtype=FORMAT, 
            channels=CHANNELS, callback=callback, latency='low'):

        while(1):
            # if sleep is added, the sound is better
            sd.sleep(1)

# profile analyse 
cProfile.run('run()')
