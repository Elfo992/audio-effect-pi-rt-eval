from numpy import exp

from tools.helper import checkOverflow

class Envelope:
    #RC time constant e^-1 = 36.8% discharged -> ln(36.8%)
    TAU = -0.999672
    MS = 0.001

    def __init__(self, attackTime: float, releaseTime: float, sampleRate, clip: bool):
        self.attackTime = attackTime        # rising time of envelope in ms
        self.releaseTime = releaseTime      # falling time of envelope in ms
        self.clip = clip                    # true if envelope should bi max 1.0
        self.lastEnvelope = 0.0
        self.sampleRate = sampleRate
        self.attackTimeCoeff = 0.0
        self.releaseTimeCoeff = 0.0
        self._calcAttackTime(attackTime)    # calc coeff of current attack time
        self._calcReleaseTime(releaseTime)  # calc coeff of current release time

    def process(self, x: float) -> float:
        pass
    
    def _calcAttackTime(self, attackTime):
        if self.attackTime == attackTime:
            return
        
        self.attackTime = attackTime
        self.attackTimeCoeff = exp(self.TAU / (self.sampleRate * attackTime * self.MS))

    def _calcReleaseTime(self, releaseTime):
        if self.releaseTime == releaseTime:
            return
        
        self.releaseTime = releaseTime
        self.releaseTimeCoeff = exp(self.TAU / (self.sampleRate * releaseTime * self.MS))


class PeakEnvelope(Envelope):
    def __init__(self, attackTime: float, releaseTime: float, sampleRate, clip: bool):
        super().__init__(attackTime, releaseTime, sampleRate, clip)

    def process(self, x: float) -> float:
        # only pos value -> rectify
        x = abs(x)

        # calc for release (falling) or attack (rising)
        # https://www.winlab.rutgers.edu/~crose/322_html/envelope_detector.html
        envelope = 0.0
        if x < self.lastEnvelope:
            envelope = self.releaseTimeCoeff * (self.lastEnvelope - x) + x
        else:
            envelope = self.attackTimeCoeff * (self.lastEnvelope - x) + x

        checkOverflow(envelope)

        if self.clip == True:
            envelope = min(envelope, 1.0)

        # reset if negative value occurs, not possible because we used only positive sample
        envelope = max(envelope, 0.0)

        self.lastEnvelope = envelope
        return envelope


        
        
