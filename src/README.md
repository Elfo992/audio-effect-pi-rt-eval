## Startup

### Starting points
The files [mainSd](mainSd.py) and [mainSdPad](mainSdPad.py) are the entry points.
- Use [mainSd](mainSd.py) for testing on a PC (Windows, Linux, ...)
- Use [mainSdPad](mainSdPad.py) for testing on the Raspberry Pi, where a 4x4 keypad is connected for switching effects

### Setup Audio I/Os
Connect your audio devices and run the [listDevices](tools/listDevices.py) script. The script prints all devices with index, detected by the *sounddevice* lib.
Use the index of your desired devices and set them inside the main-scripts.

```Python
INPUT_INDEX = 1
OUTPUT_INDEX = 0
```

### Run it
After setting the device indexes just run one of the main-scripts. If running on the pi with keypad, use the keypad to switch between effects. If running on PC, the effect is hardcoded (see ``effectId``).

### Test effects
To apply all effects on a local wave file use the script [testEffects](testEffects.py). Change the source file path and the output folder path and run it. 