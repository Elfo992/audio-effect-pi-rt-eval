import numpy as np
import wave
from scipy.io.wavfile import write as writeWav

from effects.vocoder import *
from effects.effects import *
from filters.filters import *
from effects.phaseShifter import *
from effects.filterEffects import FilterEffect

RATE = 48000
NUMBER_OF_EFFECTS = 7
inputPath = ""
outputPath = ""

# effects
effects = []
effects.append(DigitalDelay(RATE, DigitalDelayParameters(delayMs=1000)))
effects.append(ModulatedDelayFlanger(RATE, ModulatedDelayParameters(10, 7.0)))
effects.append(ModulatedDelayChorus(RATE, ModulatedDelayParameters(0.5, 30, feedbackPrc=0.0)))
effects.append(ModulatedDelayVibrato(RATE, ModulatedDelayParameters(50, 7.0, feedbackPrc=0.0)))
effects.append(PhaseVocoderRobot(512, 25))
effects.append(PhaseVocoderWhisper(512, 25))
effects.append(FilterEffect(RATE))

def applyEffect(effect, audio):
    result = []
    for x in audio:
        result.append(effect.applyEffect(float(x)))
    return np.array(result)

# read .wav file and get wave_read obj (rb)
wav = wave.open(inputPath, 'rb')

# get sample freq
sampleFreq = wav.getframerate()
print(sampleFreq)

# get number of samples
samplesNumber = wav.getnframes()
print(samplesNumber)

# read amplitudes (as bytes obj) for number of samples (reduce if lesser samples needed)
input = wav.readframes(samplesNumber)

# get actual values
input = np.frombuffer(input, dtype=np.int16)
input = input[0::2]

for i, e in enumerate(effects):
    effected = applyEffect(e, input)
    writeWav(outputPath + "output_%s.wav"%(str(type(e).__name__)), RATE, effected.astype(np.int16))