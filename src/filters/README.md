## Filters
This folder contains the implementation several filters and their corresponding coefficient calculations. Two different biquad-structures (direct, canonical) are implemented.

The file [filterCoeffs.py](filterCoeffs.py) contains coefficient calculations for the following 2nd order filters:
- Lowpass
- Highpass
- Bandpass
- Bandpass 0dB
- Notch
- Allpass

The filtering functionalities itself are capsuled inside the [filters.py](filters.py) file.