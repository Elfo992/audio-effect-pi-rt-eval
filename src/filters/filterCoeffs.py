import numpy as np
from typing import NamedTuple
from time import perf_counter

# todo: checks for i/o params eg. q factor <=0 not allowed, default = 0.707
class BiquadFilterCoeff(NamedTuple):
    # numerator coeffs
    a0: float
    a1: float
    a2: float

    # denominator coeffs
    b1: float
    b2: float

    # blending coeffs
    c0: float # processed (wet)
    d0: float # unaffected (dry)

    def __str__(self) -> str:
        return f"a0={self.a0} a1={self.a1} a2={self.a2} b1={self.b1} b2={self.b2} c0={self.c0} d0={self.d0}"

class BiquadFilterCoeffCalculatorParams:
    def __init__(self, sampleRate, fc=0.0, qFactor=0.707, boostCutGain=2, wetToDryPercentage=100):
        self.sampleRate = sampleRate
        self.fc = fc                    # cutoff and/or center frequency
        self.qFactor = qFactor
        self.boostCutGain = boostCutGain
        self.wetToDryPercentage = wetToDryPercentage

        # precalculate to safe time
        self.omega0 = 2 * np.pi * fc / sampleRate
        self.sinOmega0 = np.sin(self.omega0)
        self.cosOmega0 = np.cos(self.omega0)
        self.alpha = self.sinOmega0 / (2 * qFactor)

    # https://stackoverflow.com/questions/1227121/compare-object-instances-for-equality-by-their-attributes
    def __eq__(self, __value: object) -> bool:
        if not isinstance(__value, BiquadFilterCoeffCalculatorParams):
            # don't attempt to compare against unrelated types
            return NotImplemented
        
        return self.sampleRate == __value.sampleRate and \
            self.fc == __value.fc and \
            self.qFactor == __value.qFactor and \
            self.boostCutGain == __value.boostCutGain and \
            self.wetToDryPercentage == __value.wetToDryPercentage
    

class BiquadFilterCoeffCalculator:
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        self.params = params
        self.isDeprecated = True
        self.currentCoeffs = BiquadFilterCoeff(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        pass
    
    def changeFCorner(self, fCorner):
        self.params.fc = fCorner
        self.isDeprecated = True
        self.__refreshCoeffParams()
    
    def _addDryWetCoeffs(self):
        # calculate the dry/wet gains
        c0 = self.params.wetToDryPercentage / 100
        if c0 > 1:
            c0 = 1

        d0 = 1 - c0
        return c0, d0
    
    def __refreshCoeffParams(self):
        self.params.omega0 = 2.0 * np.pi * self.params.fc / self.params.sampleRate
        self.params.sinOmega0 = np.sin(self.params.omega0)
        self.params.cosOmega0 = np.cos(self.params.omega0)
        self.params.alpha = self.params.sinOmega0 / (2.0 * self.params.qFactor)

class BiquadLP2FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)
    
    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        if self.isDeprecated == False:
            return self.currentCoeffs
        
        start = perf_counter()
        tmpCosAlpha = (1.0 - self.params.cosOmega0) / (1.0 + self.params.alpha)

        a0 = 0.5 * tmpCosAlpha
        a1 = tmpCosAlpha
        a2 = a0
        b1 = -2.0 * self.params.cosOmega0 / (1.0 + self.params.alpha)
        b2 = (1.0 - self.params.alpha) / (1.0 + self.params.alpha)
        c0, d0 = self._addDryWetCoeffs()
        
        end = perf_counter()
        print("elapsedTime=%f" %  (end-start))

        self.isDeprecated = False
        self.currentCoeffs = BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)
        return self.currentCoeffs

class BiquadHP2FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        if self.isDeprecated == False:
            return self.currentCoeffs

        start = perf_counter()
        tmpCosAlpha = (1.0 + self.params.cosOmega0) / (1.0 + self.params.alpha)

        a0 = 0.5 * tmpCosAlpha
        a1 = -tmpCosAlpha
        a2 = 0.5* (1.0 - self.params.cosOmega0) / (1.0 + self.params.alpha)
        b1 = -2.0 * self.params.cosOmega0 / (1.0 + self.params.alpha)
        b2 = (1.0 - self.params.alpha) / (1.0 + self.params.alpha)
        c0, d0 = self._addDryWetCoeffs()

        self.isDeprecated = False
        self.currentCoeffs = BiquadFilterCoeff(a0, a1, a2, b1, b2, c0*0.1, d0) #todo check why we have to attenuate it
        
        end = perf_counter()
        print("elapsedTime=%f" %  (end-start))

        return self.currentCoeffs

class BiquadBPF2FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        if self.isDeprecated == False:
            return self.currentCoeffs

        start = perf_counter()
        tmpSinAlpha = self.params.sinOmega0 / (1.0 + self.params.alpha)

        a0 = 0.5 * tmpSinAlpha
        a1 = 0.0
        a2 = -a0
        b1 = -2.0 * self.params.cosOmega0 / (1.0 + self.params.alpha)
        b2 = (1.0 - self.params.alpha) / (1.0 + self.params.alpha)
        c0, d0 = self._addDryWetCoeffs()

        end = perf_counter()
        print("elapsedTime=%f" %  (end-start))
        
        self.isDeprecated = False
        self.currentCoeffs = BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)
        return self.currentCoeffs

class BiquadBPF20GFilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        if self.isDeprecated == False:
            return self.currentCoeffs

        start = perf_counter()
        tmpAlpha = self.params.alpha / (1.0 + self.params.alpha)

        a0 = 0.5 * tmpAlpha
        a1 = 0.0
        a2 = -a0
        b1 = -2.0 * self.params.cosOmega0 / (1.0 + self.params.alpha)
        b2 = (1.0 - self.params.alpha) / (1.0 + self.params.alpha)
        c0, d0 = self._addDryWetCoeffs()

        end = perf_counter()
        print("elapsedTime=%f" %  (end-start))
        
        self.isDeprecated = False
        self.currentCoeffs = BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)
        return self.currentCoeffs
    
class BiquadNotchFilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        if self.isDeprecated == False:
            return self.currentCoeffs

        start = perf_counter()
        a0 = 1.0 / (1.0 + self.params.alpha)
        a1 = -2.0 * self.params.cosOmega0 / (1.0 + self.params.alpha)
        a2 = a0
        b1 = a1
        b2 = (1.0 - self.params.alpha) / (1.0 + self.params.alpha)
        c0, d0 = self._addDryWetCoeffs()

        end = perf_counter()
        print("elapsedTime=%f" %  (end-start))

        self.isDeprecated = False
        self.currentCoeffs = BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)
        return self.currentCoeffs
    
class BiquadAPF2FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        if self.isDeprecated == False:
            return self.currentCoeffs

        start = perf_counter()
        a0 = (1.0 - self.params.alpha) / (1.0 + self.params.alpha)
        a1 = -2.0 * self.params.cosOmega0 / (1.0 + self.params.alpha)
        a2 = 1.0
        b1 = a1
        b2 = a0
        c0, d0 = self._addDryWetCoeffs()

        end = perf_counter()
        print("elapsedTime=%f" %  (end-start))
        
        self.isDeprecated = False
        self.currentCoeffs = BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)
        return self.currentCoeffs
    
# Pirkle S. 280 (11.33) 2nd Order Parametric EQ Filter: Constant-Q
# boostCutGain in dB
class BiquadEQConst2FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        K = np.tan(np.pi * self.params.fc / self.params.sampleRate)
        Vo = pow(10.0, self.params.boostCutGain / 20.0)
        
        d0 = 1.0 + (1.0 / self.params.qFactor) * K + K * K
        e0 = 1.0 + (1.0 / (Vo * self.params.qFactor)) * K + K * K
        alpha = 1.0 + (Vo / self.params.qFactor) * K + K * K
        beta = 2.0 * (K * K - 1.0)
        gamma = 1.0 - (Vo / self.params.qFactor) * K + K * K
        delta = 1.0 - (1.0 / self.params.qFactor) * K + K * K
        eta = 1.0 - (1.0 / (Vo * self.params.qFactor)) * K + K * K

        # check if boost (gain) or cut (attenuate)
        if self.params.boostCutGain >= 0: # boost
            a0 = alpha / d0
            a1 = beta / d0 
            a2 = gamma / d0
            b1 = beta / d0 
            b2 = delta / d0
        else: # cut
            a0 = d0 / e0
            a1 = beta / e0
            a2 = delta / e0
            b1 = beta / e0
            b2 = eta / e0
        
        c0, d0 = self._addDryWetCoeffs()
        
        self.currentCoeffs = BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)
        return self.currentCoeffs