# this file contains filter coeff. calculations based on the work of Will Pirkle 
# http://aspikplugins.com/sdkdocs/html/group___f_x-_functions.html

import numpy as np
from typing import NamedTuple

# todo: checks for i/o params eg. q factor <=0 not allowed, default = 0.707
class BiquadFilterCoeff(NamedTuple):
    # numerator coeffs
    a0: float
    a1: float
    a2: float

    # denominator coeffs
    b1: float
    b2: float

    # blending coeffs
    c0: float # processed (wet)
    d0: float # unaffected (dry)

    def __str__(self) -> str:
        return f"a0={self.a0} a1={self.a1} a2={self.a2} b1={self.b1} b2={self.b2} c0={self.c0} d0={self.d0}"

class BiquadFilterCoeffCalculatorParams:
    def __init__(self, sampleRate, fc=0.0, qFactor=0.707, boostCutGain=2, wetToDryPercentage=100):
        self.sampleRate = sampleRate
        self.fc = fc                    # cutoff and/or center frequency
        self.qFactor = qFactor
        self.boostCutGain = boostCutGain
        self.wetToDryPercentage = wetToDryPercentage

    # https://stackoverflow.com/questions/1227121/compare-object-instances-for-equality-by-their-attributes
    def __eq__(self, __value: object) -> bool:
        if not isinstance(__value, BiquadFilterCoeffCalculatorParams):
            # don't attempt to compare against unrelated types
            return NotImplemented
        
        return self.sampleRate == __value.sampleRate and \
            self.fc == __value.fc and \
            self.qFactor == __value.qFactor and \
            self.boostCutGain == __value.boostCutGain and \
            self.wetToDryPercentage == __value.wetToDryPercentage
    

class BiquadFilterCoeffCalculator:
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        self.params = params

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        pass
    
    def _addDryWetCoeffs(self):
        # calculate the dry/wet gains
        c0 = self.params.wetToDryPercentage / 100
        if c0 > 1:
            c0 = 1

        d0 = 1 - c0
        return c0, d0


# Pirkle S. 271 (11.24)
class BiquadLP1FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        theta_c = 2.0 * np.pi * self.params.fc / self.params.sampleRate
        gamma = np.cos(theta_c) / (1.0 + np.sin(theta_c))

        a0 = (1.0 - gamma) / 2.0
        a1 = (1.0 - gamma) / 2.0
        a2 = 0.0
        b1 = -gamma
        b2 = 0.0
        c0, d0 = self._addDryWetCoeffs()

        return BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)

# Pirkle S. 271 (11.24)
class BiquadHP1FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        theta_c = 2.0 * np.pi * self.params.fc / self.params.sampleRate
        gamma = np.cos(theta_c) / (1.0 + np.sin(theta_c))

        a0 = (1.0 + gamma) / 2.0
        a1 = -(1.0 + gamma) / 2.0
        a2 = 0.0
        b1 = -gamma
        b2 = 0.0
        c0, d0 = self._addDryWetCoeffs()
        
        return BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)

# Pirkle S. 272 (11.25)
class BiquadLP2FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)
    
    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        theta_c = 2.0 * np.pi * self.params.fc / self.params.sampleRate
        d = 1.0 / self.params.qFactor
        betaNumerator = 1.0 - ((d / 2.0) * (np.sin(theta_c)))
        betaDenominator = 1.0 + ((d / 2.0) * (np.sin(theta_c)))
        beta = 0.5*(betaNumerator / betaDenominator)
        gamma = (0.5 + beta) * (np.cos(theta_c))
        alpha = (0.5 + beta - gamma) / 2.0

        a0 = alpha
        a1 = 2.0 * alpha
        a2 = alpha
        b1 = -2.0 * gamma
        b2 = 2.0 * beta
        c0, d0 = self._addDryWetCoeffs()
        
        return BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)

# Pirkle S. 272 (11.25)
class BiquadHP2FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        theta_c = 2.0 * np.pi * self.params.fc / self.params.sampleRate
        d = 1.0 / self.params.qFactor
        betaNumerator = 1.0 - ((d / 2.0) * (np.sin(theta_c)))
        betaDenominator = 1.0 + ((d / 2.0) * (np.sin(theta_c)))
        beta = 0.5 * (betaNumerator / betaDenominator)
        gamma = (0.5 + beta) * (np.cos(theta_c))
        alpha = (0.5 + beta + gamma) / 2.0

        a0 = alpha
        a1 = -2.0 * alpha
        a2 = alpha
        b1 = -2.0 * gamma
        b2 = 2.0 * beta
        c0, d0 = self._addDryWetCoeffs()
        
        return BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)

# Pirkle S. 273 (11.26)
class BiquadBPF2FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        k = np.tan(np.pi * self.params.fc / self.params.sampleRate)
        delta = k * k * self.params.qFactor + k + self.params.qFactor

        a0 = k / delta
        a1 = 0.0
        a2 = -k / delta
        b1 = 2.0 * self.params.qFactor * (k * k - 1) / delta
        b2 = (k * k * self.params.qFactor - k + self.params.qFactor) / delta
        c0, d0 = self._addDryWetCoeffs()
        
        return BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)

# Pirkle S. 273 (11.26)
class BiquadBSF2FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        k = np.tan(np.pi * self.params.fc / self.params.sampleRate)
        delta = k * k * self.params.qFactor + k + self.params.qFactor

        a0 = self.params.qFactor * (1 + k * k) / delta
        a1 = 2.0 * self.params.qFactor * (k * k - 1) / delta
        a2 = self.params.qFactor * (1 + k * k) / delta
        b1 = 2.0 * self.params.qFactor * (k * k - 1) / delta
        b2 = (k * k * self.params.qFactor - k + self.params.qFactor) / delta
        c0, d0 = self._addDryWetCoeffs()
        
        return BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)

# Pirkle S. 276
class BiquadAPF1FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        alphaNum = np.tan((np.pi * self.params.fc) / self.params.sampleRate) - 1.0
        alphaDenom = np.tan((np.pi * self.params.fc) / self.params.sampleRate) + 1.0
        alpha = alphaNum / alphaDenom

        a0 =  alpha
        a1 =  1.0
        a2 =  0.0
        b1 =  alpha
        b2 =  0.0
        c0, d0 = self._addDryWetCoeffs()
        
        return BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)
    

# Pirkle S. 277 (11.30) for crossovers and reverb algorithms
class BiquadAPF2FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        theta_c = 2.0 * np.pi * self.params.fc / self.params.sampleRate
        bw = self.params.fc / self.params.qFactor 
        argTan = np.pi * bw / self.params.sampleRate

        # check for tan "overflow"
        if argTan >= 0.95 * np.pi / 2.0:
            argTan = 0.95 * np.pi / 2.0

        alphaNumerator = np.tan(argTan) - 1.0
        alphaDenominator = np.tan(argTan) + 1.0
        alpha = alphaNumerator / alphaDenominator
        beta = -np.cos(theta_c)

        a0 =  -alpha
        a1 =  beta * (1.0 - alpha)
        a2 =  1.0
        b1 =  beta * (1.0 - alpha)
        b2 =  -alpha
        c0, d0 = self._addDryWetCoeffs()
        
        return BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)
    
# Pirkle S. 280 (11.33) 2nd Order Parametric EQ Filter: Constant-Q
# boostCutGain in dB
class BiquadEQConst2FilterCoeffCalculator(BiquadFilterCoeffCalculator):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        super().__init__(params)

    def getFilterCoeffs(self) -> BiquadFilterCoeff:
        K = np.tan(np.pi * self.params.fc / self.params.sampleRate)
        Vo = pow(10.0, self.params.boostCutGain / 20.0)
        
        d0 = 1.0 + (1.0 / self.params.qFactor) * K + K * K
        e0 = 1.0 + (1.0 / (Vo * self.params.qFactor)) * K + K * K
        alpha = 1.0 + (Vo / self.params.qFactor) * K + K * K
        beta = 2.0 * (K * K - 1.0)
        gamma = 1.0 - (Vo / self.params.qFactor) * K + K * K
        delta = 1.0 - (1.0 / self.params.qFactor) * K + K * K
        eta = 1.0 - (1.0 / (Vo * self.params.qFactor)) * K + K * K

        # check if boost (gain) or cut (attenuate)
        if self.params.boostCutGain >= 0: # boost
            a0 = alpha / d0
            a1 = beta / d0 
            a2 = gamma / d0
            b1 = beta / d0 
            b2 = delta / d0
        else: # cut
            a0 = d0 / e0
            a1 = beta / e0
            a2 = delta / e0
            b1 = beta / e0
            b2 = eta / e0
        
        c0, d0 = self._addDryWetCoeffs()
        
        return BiquadFilterCoeff(a0, a1, a2, b1, b2, c0, d0)