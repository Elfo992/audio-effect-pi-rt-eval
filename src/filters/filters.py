from filters.filterCoeffs import *
from tools.helper import checkOverflow

class Filter:
    def applyFilter(self, x: float) -> float:
        pass

class BiquadFilterBuffer:
    def __init__(self):
        self.x1 = float(0.0)    # x[n-1]
        self.x2 = float(0.0)    # x[n-2]
        self.y1 = float(0.0)    # y[n-1]
        self.y2 = float(0.0)    # y[n-2]

class DirectBiquadFilter(Filter):

    # 100% = only processed data, 0% = only unaffected data
    def __init__(self, coefficientsCalculator: BiquadFilterCoeffCalculator):
        self.coeffCalc = coefficientsCalculator
        self.coefficients = coefficientsCalculator.getFilterCoeffs()
        self.buffer = BiquadFilterBuffer()
        self.delayedSum = 0.0

    def applyFilter(self, x: float) -> float:
        pass

    def changeFCorner(self, fCorner):
        self.coeffCalc.changeFCorner(fCorner)

    def _applyFilter(self, x: float) -> float:
        self.coefficients = self.coeffCalc.getFilterCoeffs() 
        
        # needed for phase
        self.delayedSum = self.coefficients.a1 * self.buffer.x1 + self.coefficients.a2 * self.buffer.x2 - \
            self.coefficients.b1 * self.buffer.y1 - self.coefficients.b2 * self.buffer.y2

        y = self.coefficients.a0 * x + self.delayedSum

        # add dry/wet factors
        y = self.coefficients.c0 * y + self.coefficients.d0 * x

        # update buffer
        self.buffer.x2 = self.buffer.x1
        self.buffer.x1 = x
        self.buffer.y2 = self.buffer.y1
        self.buffer.y1 = y

        return checkOverflow(y)
    
    def getA0Coefficient(self)-> float:
        return self.coefficients.a0
    
    def getDelayedSum(self)-> float:
        return self.delayedSum

# canonical
class CanonicalBiquadFilter(Filter):
    # 100% = only processed data, 0% = only unaffected data
    def __init__(self, coefficientsCalculator: BiquadFilterCoeffCalculator):
        self.coeffCalc = coefficientsCalculator
        self.coefficients = coefficientsCalculator.getFilterCoeffs()
        self.buffer = BiquadFilterBuffer()

    def applyFilter(self, x: float) -> float:
        pass

    def changeFCorner(self, fCorner):
        self.coeffCalc.changeFCorner(fCorner)

    def _applyFilter(self, x: float) -> float:
        self.coefficients = self.coeffCalc.getFilterCoeffs()

        # w[n] node in canonical biquad
        w = x - self.coefficients.b1 * self.buffer.x1 - self.coefficients.b2 * self.buffer.x2
        y = self.coefficients.a0 * w + self.coefficients.a1 * self.buffer.x1 + self.coefficients.a2 * self.buffer.x2

        # add dry/wet factors
        y = self.coefficients.c0 * y + self.coefficients.d0 * x

        # update buffer
        self.buffer.x2 = self.buffer.x1
        self.buffer.x1 = w

        return checkOverflow(y)
    
    def getA0Coefficient(self)-> float:
        return self.coefficients.a0
    
class BiquadLP2Filter(DirectBiquadFilter):
    def __init__(self,params: BiquadFilterCoeffCalculatorParams):
        coeffCalculator = BiquadLP2FilterCoeffCalculator(params)
        super().__init__(coeffCalculator)

    def applyFilter(self, x):
        return super()._applyFilter(x)
    
class BiquadHP2Filter(CanonicalBiquadFilter):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        coeffCalculator = BiquadHP2FilterCoeffCalculator(params)
        super().__init__(coeffCalculator)

    def applyFilter(self, x):
        return super()._applyFilter(x)
    
class BiquadBPF2Filter(DirectBiquadFilter):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        coeffCalculator = BiquadBPF2FilterCoeffCalculator(params)
        super().__init__(coeffCalculator)

    def applyFilter(self, x):
        return super()._applyFilter(x)
    
# bandpass with 0dB
class BiquadBPF20GFilter(DirectBiquadFilter):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        coeffCalculator = BiquadBPF20GFilterCoeffCalculator(params)
        super().__init__(coeffCalculator)

    def applyFilter(self, x):
        return super()._applyFilter(x)

class BiquadNotchFilter(DirectBiquadFilter):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        coeffCalculator = BiquadNotchFilterCoeffCalculator(params)
        super().__init__(coeffCalculator)
    
    def applyFilter(self, x):
        return super()._applyFilter(x)

class BiquadAPF2Filter(DirectBiquadFilter):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        coeffCalculator = BiquadAPF2FilterCoeffCalculator(params)
        super().__init__(coeffCalculator)

    def applyFilter(self, x):
        return super()._applyFilter(x)

# info: boostCutGainDb = -12dB - +12dB, Q = 1
class BiquadEQConst2Filter(DirectBiquadFilter):
    def __init__(self, params: BiquadFilterCoeffCalculatorParams):
        coeffCalculator = BiquadEQConst2FilterCoeffCalculator(params)
        super().__init__(coeffCalculator)

    def applyFilter(self, x):
        return super()._applyFilter(x)